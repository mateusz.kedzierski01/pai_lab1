﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PAI_Lab1.Glowna" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="height: 100%">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="Properties/styl.css" rel="stylesheet" type="text/css" />
    <title>Strona Główna</title>
</head>

<body>
    <form runat="server">
            
        <div class="container">
            <div class="controls">
                <p>
                    <asp:Label runat="server" Text="Zmiana rysunku:"/>
                </p>

                <p>
                    <asp:Button ID="prev_butt" CssClass="button" OnClick="prev" runat="server" Text="Poprzedni" />
                </p>

                <p>
                    <asp:Button ID="next_butt" CssClass="button" OnClick="next" runat="server" Text="Następny" />
                </p>
            </div>

            <div class="view">
                    <asp:ImageButton CssClass="image" runat="server" ID="image" onClick="On_Click"/>
            </div>
        </div>
        
    </form>
</body>

</html>