﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PAI_Lab1
{
    public partial class Glowna : System.Web.UI.Page
    {
        private static int pos = 0;
        private static int im_number = 0;
        private static String[] urls = { "Properties/picture1.jpg", "Properties/picture2.jpg", "Properties/picture3.jpg", "Properties/picture4.jpg" };

        protected void Page_Load(object sender, EventArgs e)
        {
            prev_butt.Enabled = false;
            image.ImageUrl = urls[im_number];
            check();
        }

        private void check()
        {
            if(im_number==0) prev_butt.Enabled = false;
            else prev_butt.Enabled = true;

            if (im_number == 3) next_butt.Enabled = false;
            else next_butt.Enabled = true;
        }

        protected void prev(object sender, EventArgs e)
        {
            im_number--;
            image.ImageUrl = urls[im_number];
            check();
        }
        protected void next(object sender, EventArgs e)
        {
            im_number = im_number+1;
            image.ImageUrl = urls[im_number];
            check();
        }

        protected void On_Click(object sender, ImageClickEventArgs e)
        {

            if (pos == 0)
            {
                image.CssClass = "image i1";
            } 
            else if (pos == 1)
            {
                image.CssClass = "image i2";
            }
            else if (pos == 2)
            {
                image.CssClass = "image i3";
            }
            else if (pos == 3)
            {
                image.CssClass = "image i4";
            }
            else if (pos == 4)
            {
                image.CssClass = "image i0";
            }

            pos = (pos + 1) % 5;
            
        }
    }
}